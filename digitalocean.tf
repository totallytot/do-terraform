variable "do_token" {}

provider "digitalocean" {
  token = "${var.do_token}"
}

resource "digitalocean_tag" "info" {
  name = "totally_tot-root"
}

resource "digitalocean_ssh_key" "mine" {
  name       = "Totally Tot public key"
  public_key = "${file("d:/id_rsa.pub")}"
}

data "digitalocean_ssh_key" "rebrainme" {
  name = "Rebrain's key"
}

resource "digitalocean_droplet" "web" {
  image  = "ubuntu-18-04-x64"
  name   = "totally-tot-droplet"
  region = "nyc3"
  size   = "s-1vcpu-1gb"
  ssh_keys = ["${digitalocean_ssh_key.mine.fingerprint}"]
  tags   = ["${digitalocean_tag.info.id}"]
}